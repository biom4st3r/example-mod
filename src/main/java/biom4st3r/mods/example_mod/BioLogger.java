package biom4st3r.mods.example_mod;

import java.util.logging.ConsoleHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import net.fabricmc.loader.api.FabricLoader;


public class BioLogger
{
    protected Logger logger;
    private static ConsoleHandler ch;

    public static final String BLACK = "\033[0;30m";   // BLACK
    public static final String RED = "\033[0;31m";     // RED
    public static final String GREEN = "\033[0;32m";   // GREEN
    public static final String YELLOW = "\033[0;33m";  // YELLOW
    public static final String BLUE = "\033[0;34m";    // BLUE
    public static final String PURPLE = "\033[0;35m";  // PURPLE
    public static final String CYAN = "\033[0;36m";    // CYAN
    public static final String WHITE = "\033[0;37m";   // WHITE

    public BioLogger(String MODID)
    {
        this.logger = Logger.getLogger(MODID);
        logger.setUseParentHandlers(false);
        logger.addHandler(ch);
        ch.setLevel(Level.ALL);
        if(FabricLoader.getInstance().isDevelopmentEnvironment())
        {
            this.logger.setLevel(Level.ALL);
        }
        else
        {
            this.logger.setLevel(Level.INFO);
        }
    }

    public static Logger createLogger(String MODID)
    {
        Logger tlog = Logger.getLogger(MODID);
        tlog.setUseParentHandlers(false);
        tlog.addHandler(ch);
        ch.setLevel(Level.ALL);
        if(FabricLoader.getInstance().isDevelopmentEnvironment())
        {
            tlog.setLevel(Level.ALL);
        }
        else
        {
            tlog.setLevel(Level.INFO);
        }
        return tlog;
    }

    public void setDebug(Level lvl)
    {
        this.logger.setLevel(lvl);
    }

    public void lesserDebug(String format, Object... args)
    {
        this.logger.fine(String.format(format, args));
    }

    public void debug(String format, Object... args)
    {
        this.logger.config(String.format(format, args));
    }

    public void log(String format, Object... args)
    {
        this.logger.info(String.format(format, args));

    }

    public void error(String format, Object... args)
    {
        this.logger.warning(String.format(format, args));
    }

    static
    {
        ch = new ConsoleHandler();
        ch.setFormatter(new Formatter(){
        
            @Override
            public String format(LogRecord lr) {
                Thread thread = Thread.currentThread();
                String color;// = lr.getLevel().intValue() == 800 ? "\u001B[37m" : "";
                switch(lr.getLevel().intValue())
                {
                    case 600:
                        color = BLUE;
                        break;
                    case 700:
                        color =  GREEN;
                        break;
                    case 800:
                        color =  WHITE;
                        break;
                    case 900:
                        color = RED;
                        break;
                    default:
                        color =  YELLOW;
                        break;
                }
                String output =String.format(color + "[%s/%s][%s] %s", 
                thread.getName(),
                lr.getLevel() != Level.CONFIG ? lr.getLevel() : "DEBUG",
                lr.getLoggerName(),
                lr.getMessage());
                return output.replace("\n", "\n"+color)+"\n";
            }
        });
    }
}